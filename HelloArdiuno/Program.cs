﻿using Raspberry.IO.GeneralPurpose;
using Raspberry.IO.InterIntegratedCircuit;
using System;
using System.Threading;

namespace HelloArdiuno
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Hello Arduino");

			const ConnectorPin sdaPin = ConnectorPin.P1Pin03;
			const ConnectorPin sclPin = ConnectorPin.P1Pin05;

			var config = System.Configuration.ConfigurationManager.GetSection("gpioConnection") as Raspberry.IO.GeneralPurpose.Configuration.GpioConnectionConfigurationSection;
			Console.WriteLine($"BoardConnectorRevision: {config?.BoardConnectorRevision}");

			using (var driver = new I2cDriver(sdaPin.ToProcessor(), sclPin.ToProcessor()))
			{
				// Open a connection - Connect to Device 0x04
				var connection = driver.Connect(0x04);

				Action<int> writeValue = (value) => connection.WriteByte(Convert.ToByte(value));

				Func<int> readValue = () => connection.ReadByte();

				string input = PromptForInput();

				while (input.ToLowerInvariant() != "exit")
				{
					var number = Int32.Parse(input);
					writeValue(number);
					Console.WriteLine($"RPI: Hi Arduino, I sent you {number}");
					number = 0;

					Thread.Sleep(1000);

					number = readValue();
					Console.WriteLine($"Arduino: Hey RPI, I received a digit {number}");

					input = PromptForInput();
				}
				Console.WriteLine("Exiting...");
			}
		}

		private static string PromptForInput()
		{
			Console.WriteLine("Enter 1 - 9: ");
			return Console.ReadLine();
		}
	}
}
