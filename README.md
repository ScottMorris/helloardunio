#Hello Arduino

This is a test project to see if I can get Mono to work with the Raspberry Pi and the Arduino.

##Notes

This project contains a subtree from Raspberry Sharp's [GitHub Repo](https://github.com/raspberry-sharp/raspberry-sharp-io)

For more intformation see the Atlassian Blog post [Alternatives To Git Submodule: Git Subtree](http://blogs.atlassian.com/2013/05/alternatives-to-git-submodule-git-subtree/)